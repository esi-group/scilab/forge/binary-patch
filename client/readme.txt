I've developed this PoC for GSoC on msvc, so I'm not sure code compiles on other compilers/os.

Add update.vc(x)proj to the solution.
Apply callinterf.h.diff and callinterf.c.diff
Run a webserver w/ PHP support.
Copy the files from patching_server/ into your http docroot somewhere.

Open SCILAB/modules/update/sci_gateway/cpp/sci_update.cpp and replace the line:
	std::string const update_url("http://127.0.0.1/downloadupdates.php");
with the right url to your http server.

Currently, I'm using std::cout/std::cerr for output in the upload module.


Note: If you have trouble using my .vcproj (or you're using autotools),
	you will need to create your own project from existing code.
Files to be included in the project are:
update/src/cpp/*.cpp
update/src/c/*.c
update/sci_gateway/cpp/*.cpp
update/sci_gateway/c/*.c
update/includes/*.h,*.hxx (you'll need to remove the
	update/includes/curl/*.h afterwards)

Some other settings are:
	1) Add reference core to the project update
	2) Add $(SolutionDir)modules\core\includes;$(SolutionDir)modules\update\includes;  to Additional Include Directories
	3) Add $(SolutionDir)\modules\update\libs to Additional Link Directories
	4) Add libcurl.lib;mspatcha.lib to Additional Dependencies
	5) Change the Output File to $(SolutionDir)bin\$(TargetName)$(TargetExt)


It is also possible that you encounter problems with linking to mspatcha.lib.
For some reason, although mspatcha.dll comes with windows, there is no mspatcha.lib anywhere,
in order to perform implicit dynamic linking. Therefore, I generated my own .def and .lib
from the dll. However, there may be problems, since dumpbin was only able to generate .def-s
with _cdecl function names, whereas the functions in mspatcha.dll were _stdcall.
I modified all the entries in the .def as to use stdcall calling convention instead and it
works fine here, but you might have to recreate the lib on your os.

Build and cross your fingers!
