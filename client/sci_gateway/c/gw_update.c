#include "gw_update.h"
#include "callFunctionFromGateway.h"


static gw_generic_table Tab[] =
{
	{sci_update, "update_scilab"}
};

int gw_update(void)
{
	callFunctionFromGateway(Tab, SIZE_CURRENT_GENERIC_TABLE(Tab));
	return 0;
}