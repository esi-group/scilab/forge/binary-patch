#include "xdelta_updater.hxx"
#include <sstream>
#include <cstdlib>

unsigned int xdelta_updater::UPDATER_ID = 3;

bool xdelta_updater::apply_update (std::string const &filename,
		std::string const &patchfile)
{
	std::stringstream ss;
	
	//xdelta provides a C/C++ api, but we'll stick with the binary for now
	//this can easily be modified to work for linux
	std::string const xdelta_app = "../modules/update/tools/xdelta3.0z.x86-64.exe";
	ss << xdelta_app << " -d -s " << filename << ' ' << patchfile << ' '
		<< filename << " 1>nul 2>nul";
	return std::system(ss.str().c_str()) == 0;
}

namespace
{
	updater *create_xdelta_updater() { return new xdelta_updater; }
	
	static bool xdelta_updater_dummy_value =
		updater_manager::get_instance().register_updater(
			xdelta_updater::UPDATER_ID,
			&create_xdelta_updater);
}