#if !defined(INCLUDED_SEND_WHOLE_FILE_UPDATER_HXX)
#define INCLUDED_SEND_WHOLE_FILE_UPDATER_HXX

#include "updater.hxx"

//! A simple updater that works by simply sending the whole file.
class send_whole_file_updater : public updater
{
public:
	static unsigned int UPDATER_ID;
	bool apply_update (std::string const &filename,
		std::string const &patchfile);
	std::string get_updater_name () const { return "Send whole file updater"; }
	unsigned int get_updater_id() const { return UPDATER_ID; }
};

#endif