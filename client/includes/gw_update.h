#if !defined(INCLUDED_GW_UPDATE_H)
#define INCLUDED_GW_UPDATE_H

__declspec(dllexport) int gw_update(void);

__declspec(dllexport) int sci_update(
	char *fname,unsigned long fname_len);

#endif