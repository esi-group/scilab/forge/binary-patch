#if !defined(INCLUDED_UPDATER_HXX)
#define INCLUDED_UPDATER_HXX

#include <cassert>
#include <map>

//!An abstract class that all updater classes have to implement
class updater
{
public:
	virtual bool apply_update (std::string const &filename,
		std::string const &patchfile) = 0;
	virtual std::string get_updater_name () const = 0;
	virtual unsigned int get_updater_id() const = 0;

protected:
	updater() { }
	virtual ~updater() { }
};

//!A singleton class that keeps track of all the available updaters.
//!Inspired from the Object Factory idiom from "Thinking in C++"
class updater_manager
{
public:
	//singleton
	static updater_manager &get_instance()
	{
		static updater_manager inst;
		return inst;
	}

	typedef updater *(*create_updater_callback)();
	typedef std::map<unsigned int, create_updater_callback>
		updaters_map;

	bool register_updater (unsigned int updater_id,
		create_updater_callback updater_creator)
	{ updaters_[updater_id] = updater_creator; return true; }

	updater *create_updater (unsigned int updater_id)
	{
		assert(updaters_.find(updater_id) != updaters_.end());
		return updaters_[updater_id]();
	}
	
	updaters_map const &get_updaters() const
	{
		return updaters_;
	}

	bool are_any_updaters() const
	{
		return !updaters_.empty();
	}
private:
	updaters_map updaters_;

	//singleton
	updater_manager() { }
	~updater_manager() { }
};



#endif