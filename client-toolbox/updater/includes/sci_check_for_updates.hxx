
#ifndef  INCLUDED_SCI_CHECK_FOR_UPDATES_HXX_INC
#define  INCLUDED_SCI_CHECK_FOR_UPDATES_HXX_INC

#include	"gateway_export.hxx"

extern "C" UPDATER_GATEWAY_EXPORT int sci_check_for_updates (char *fname);

#endif   /* ----- #ifndef INCLUDED_SCI_CHECK_FOR_UPDATES_HXX_INC  ----- */
