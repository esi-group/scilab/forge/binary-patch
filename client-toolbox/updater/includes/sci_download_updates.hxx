
#ifndef  SCI_DOWNLOAD_UPDATES_HXX_INC
#define  SCI_DOWNLOAD_UPDATES_HXX_INC

#include	"gateway_export.hxx"

extern "C" UPDATER_GATEWAY_EXPORT int sci_download_updates (char *fname);

#endif   /* ----- #ifndef SCI_DOWNLOAD_UPDATES_HXX_INC  ----- */
