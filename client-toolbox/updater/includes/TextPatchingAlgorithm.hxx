#ifndef  INCLUDED_TEXTPATCHINGALGORITHM_HXX_INC
#define  INCLUDED_TEXTPATCHINGALGORITHM_HXX_INC

#include <string>
#include "PatchingAlgorithm.hxx"

//! An updater that can be used for text files (text diff)
class TextPatchingAlgorithm : public PatchingAlgorithm
{
public:
    static unsigned int ALGORITHM_ID;
    TextPatchingAlgorithm() { }

    bool applyUpdate (std::string const &filename,
            std::string const &patchfile);

    unsigned int getPatchingAlgorithmId() const { return ALGORITHM_ID; }

    std::string getPatchingAlgorithmName () const
    { return "Text diff"; }

    virtual ~TextPatchingAlgorithm() { }
};
#endif   /* ----- #ifndef INCLUDED_TEXTPATCHINGALGORITHM_HXX_INC  ----- */
