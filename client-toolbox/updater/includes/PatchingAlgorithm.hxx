#if !defined(INCLUDED_UPDATER_PATCHING_ALGORITHM_HXX)
#define INCLUDED_UPDATER_PATCHING_ALGORITHM_HXX

#include	<string>

//!An abstract class that all patching algorithms classes have to implement
class PatchingAlgorithm
{
public:
	virtual bool applyUpdate (std::string const &filename,
		std::string const &patchfile) = 0;
	virtual std::string getPatchingAlgorithmName () const = 0;
	virtual unsigned int getPatchingAlgorithmId() const = 0;

	virtual ~PatchingAlgorithm() { }
protected:
	PatchingAlgorithm() { }
};

#endif
