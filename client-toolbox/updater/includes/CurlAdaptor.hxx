
#ifndef  INCLUDED_CURL_ADAPTOR_HXX_INC
#define  INCLUDED_CURL_ADAPTOR_HXX_INC

#include	<curl/curl.h>
#include	<string>
#include	<exception>
#include	<stdexcept>
#include	<cctype>
#include	<memory>
#include	"gateway_export.hxx"

class UPDATER_IMPL_SRC_EXPORT_IMPORT CurlEasy
{
public:
    static std::string urlencode (std::string const &str);
protected:
    CurlEasy ()
    {
        initialize_libcurl();
        curl = curl_easy_init();
        curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, error);
        error[0] = 0;
    }

    ~CurlEasy ()
    {
        curl_easy_cleanup(curl);
    }

    bool curlPerform ()
    {
        return curl_easy_perform(curl) == 0;
    }

    bool curlPerform (std::string const &url)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        return curlPerform();
    }

    char const *getCurlError() const { return error; }

    CURL *curl;
private:
    //!@TODO: should we ever call curl_global_cleanup()?
    static void initialize_libcurl ();
	static bool initialized_libcurl;
    char error[CURL_ERROR_SIZE];
};

class UPDATER_IMPL_SRC_EXPORT_IMPORT CurlException : public std::exception
{
public:
    CurlException (const char *msg) : msg_(msg) { }
    virtual const char *what() const throw() { return msg_.c_str(); }
    virtual ~CurlException() throw() { }
private:
    std::string msg_;
};

//@TODO: allocator?
template <class Derived>
class UPDATER_IMPL_SRC_EXPORT_IMPORT CurlAdaptor : public CurlEasy
{
protected:
    CurlAdaptor () : CurlEasy()
    {
        curl_easy_setopt(CurlEasy::curl, CURLOPT_WRITEFUNCTION,
                &CurlAdaptor::got_data);
        curl_easy_setopt(CurlEasy::curl, CURLOPT_WRITEDATA, &curl_data_);
    }

    void onDownloadComplete (std::string &data)
    {
        static_cast<Derived*>(this)->onDownloadComplete(data);
    }

    void curlPerform (std::string const &url)
    {
        curl_data_.clear();
        bool ret = CurlEasy::curlPerform(url);
        if (!ret) throw CurlException(CurlEasy::getCurlError());
        onDownloadComplete(curl_data_);
    }

    void setPostData (std::string const &post)
    {
        post_data_.reset( new std::string(post) );
        curl_easy_setopt(CurlEasy::curl, CURLOPT_POST, 1);
        curl_easy_setopt(CurlEasy::curl,
                CURLOPT_POSTFIELDS, post_data_->c_str());
        curl_easy_setopt(CurlEasy::curl, CURLOPT_POSTFIELDSIZE, post.size());
    }

    void setUserAgent (std::string const &user_agent)
    {
        curl_easy_setopt(CurlEasy::curl,
                CURLOPT_USERAGENT, user_agent.c_str());
    }
private:
    std::string curl_data_;
    std::auto_ptr<std::string> post_data_;
	static std::size_t got_data
        (char *bufptr, size_t size, size_t nitems, void *userp)
    {
        if (size * nitems == 0) return 0;
        std::string &data = *reinterpret_cast<std::string*>(userp);
        data.append(bufptr, bufptr+size*nitems);
        return size*nitems;
    }
};

#endif   /* ----- #ifndef INCLUDED_CURL_ADAPTOR_HXX_INC  ----- */
