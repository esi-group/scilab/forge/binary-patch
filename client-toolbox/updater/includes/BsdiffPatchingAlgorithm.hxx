
#ifndef  INCLUDED_BSDIFFPATCHINGALGORITHM_HXX_INC
#define  INCLUDED_BSDIFFPATCHINGALGORITHM_HXX_INC

#include	"PatchingAlgorithm.hxx"

class BsdiffPatchingAlgorithm : public PatchingAlgorithm
{
public:
    static unsigned int ALGORITHM_ID;
    bool applyUpdate (std::string const &filename,
            std::string const &patchfile);
    std::string getPatchingAlgorithmName () const 
        { return "Bspatch patching algorithm"; }
    unsigned int getPatchingAlgorithmId() const { return ALGORITHM_ID; }

    virtual ~BsdiffPatchingAlgorithm() { }
};

#endif   /* ----- #ifndef INCLUDED_BSDIFFPATCHINGALGORITHM_HXX_INC  ----- */
