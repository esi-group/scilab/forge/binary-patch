
#ifndef  INCLUDED_UPDATER_UPDATERTHREAD_HXX_INC
#define  INCLUDED_UPDATER_UPDATERTHREAD_HXX_INC

//@TODO: include less
#include	<boost/thread.hpp>
#include	"common.hxx"
#include	"gateway_export.hxx"

//@TODO: somehow refuse to spawn this thread if this thread is spawned
//elsewhere (in other words, if multiple instances of scilab are run, we only
//want one instance of this thread to run)
class UPDATER_IMPL_SRC_EXPORT_IMPORT UpdaterThread
{
    //@TODO: update comments + add all possible return codes for each function
    //@TODO: deal with situations when the thread is not running
public:
    /*
     * Gets the only instance of this class.
     */
    static UpdaterThread &getInstance()
    {
        static UpdaterThread updaterThread;
        return updaterThread;
    }
    
    enum ErrorCode
    {
        Ok,
        ThreadNotRunning,
        ThreadIsRunning,
        ThreadResourceError,
        HaveDownloadedUnappliedUpdates,
        AlreadyCheckingForUpdates,
        WasNotAskingForDownloadConfirmation,
        NoUpdatesAvailable,
        SeeUpdaterException,
        ThreadBusy
    };

    /*
     * Starts the thread that periodically checks for updates.
     * This shouldn't be run when there are downloaded, but unapplied updates,
     * otherwise we will end up downloading the same update more than once. Use
     * UnappliedUpdatesStore::areUnappliedUpdates() to check.
     * Run from: Main thread.
     */
    ErrorCode runPeriodicalUpdatesChecker();

    bool isRunning();

    /*
     * Stops the periodical updates checker (kills the thread).
     * Run from: Main thread.
     */
    bool stopPeriodicalUpdatesChecker();

    /*
     * Send a message to the updater thread, requiring it to check for updates
     * now.
     * Run from: Main thread.
     */
    ErrorCode forceUpdatesCheck();


    /*
     * Sends a message to the updater thread, requiring it to check for updates
     * now. It then waits for the updater thread to complete checking for
     * updates, and then makes the list available to the main thread using the
     * function
     * CheckForUpdates::getInstance().getAvailableUpdates().
     * After calling this function and using getAvailableUpdates(), you MUST
     * call resume() in order to allow the updater thread to keep doing its
     * work (its work is paused after the call to forceUpdatesCheckAndWait,
     * because it offers exclusive access to main thread to
     * getAvailableUpdates()). If you don't call resume() afterward, the
     * updater thread will hang doing nothing.
     */
    ErrorCode forceUpdatesCheckAndWait(bool giveOwnershipUntilResume);
    
    ErrorCode forceUpdatesCheckAndDownloadAndWait();

    void resume();

    //!automatically calls resume() on the UpdaterThread object whenever
    //!the ScopedResume object runs out of scope (is dtor-ed).
    struct ScopedResume
    {
        ScopedResume () { }
        ~ScopedResume() { UpdaterThread::getInstance().resume(); }
    };

    //@TODO: impl
    ErrorCode refuseDownloadUpdates();

    ErrorCode allowDownloadUpdates();

    ErrorCode allowDownloadUpdatesAndWait();

    void setExpectingDownloadConfirmation(bool);

    bool isWaitingDownloadConfirmation();

    //only meant to be called from main thread
    std::string getMessageFromErrorCode (ErrorCode ec);

    //@TODO: this assumes copying is done before unwinding, so lock_guard is
    //destructed after the copy 
    UpdaterException getLastUpdaterException() const
    { 
        return last_updater_exception_;
    }

private:
    ErrorCode refuse_or_allow_download_updates (bool allow);

    UpdaterException last_updater_exception_;

    bool is_running_;
    boost::mutex is_running_mutex_;

    //updater thread uses this to know whether it should ask the main thread
    //before downloading an update.
    //main thread writes in this variable, updater thread reads from it
    bool expecting_download_confirmation_;
    boost::mutex expecting_download_confirmation_mutex_;

    //updater thread uses this to know whether it should report to the main
    //thread when it's done checking for updates
    //main thread writes in this variable, updater thread reads from it
    bool expecting_updates_check_report_;
    boost::mutex expecting_updates_check_report_mutex_;

    //updater thread uses this to know whether it should report to the main
    //thread when it has completed downloading updates.
    //main thread writes in this variable, updater thread reads from it
    bool expecting_download_complete_report_;
    boost::mutex expecting_download_complete_report_mutex_;

    //updater thread uses this to let the main thread know that it is currently
    //in the process of downloading an update
    bool is_currently_checking_for_updates_;
    boost::mutex is_currently_checking_for_updates_mutex_;

    //updater thread uses this to inform main thread that it has completed
    //downloading updates (when expecting_download_complete_report_) is
    //enabled.
    //updater thread writes in this variable, main thread reads from it
    bool download_complete_report_;
    UpdaterException download_complete_report_result_;
    boost::mutex download_complete_report_mutex_;
    boost::condition_variable download_complete_report_cond_;

    //updater thread uses this to inform main thread that it's done checking
    //for updates (when expecting_updates_check_report_ is enabled).
    //updater thread writes in this variable, main thread reads from it
    bool done_checking_for_updates_;
    UpdaterException done_checking_for_updates_result_;
    boost::mutex done_checking_for_updates_mutex_;
    boost::condition_variable done_checking_for_updates_cond_;

    //main thread uses this to allow the updater thread to resume checking for
    //updates after a done_checking_for_updates_mutex_
    bool resume_checking_for_updates_;
    boost::mutex resume_checking_for_updates_mutex_;
    boost::condition_variable resume_checking_for_updates_cond_;


    //main thread uses this to ask the updater thread to check for updates now.
    //main thread writes in this variable, updater thread reads from it
    //note that both bools are the responsibility of the same mutex
    bool allowing_forced_updates_check_;
    bool is_forcing_updates_check_;
    boost::mutex is_forcing_updates_check_mutex_;
    boost::condition_variable is_forcing_updates_check_cond_;

    //updater thread uses this to inform main thread that confirmation of
    //downloading updates is needed.
    //updater thread writes in this variable, main thread reads from it
    bool is_asking_for_download_confirmation_;
    boost::mutex is_asking_for_download_confirmation_mutex_;
    boost::condition_variable is_asking_for_download_confirmation_cond_;

    //main thread uses this to inform updater thread whether downloading
    //updates was allowed or denied.
    //main thread writes in this variable, updater thread reads from it
    //note that both bools are the responsibility of the same mutex
    bool confirmation_allow_download_response_;
    bool confirmation_allow_download_;
    boost::mutex confirmation_allow_download_mutex_;
    boost::condition_variable confirmation_allow_download_cond_;

    UpdaterThread ();
    UpdaterThread (UpdaterThread const &);
    UpdaterThread &operator= (UpdaterThread const &);
    //@TODO: this might block, need some kind of interrupt
    ~UpdaterThread() { if (thread_.joinable()) thread_.join(); }

    void on_found_updates();
    boost::thread thread_;
    
public:
    void operator()();

};

#endif   /* ----- #ifndef INCLUDED_UPDATER_UPDATERTHREAD_HXX_INC  ----- */
