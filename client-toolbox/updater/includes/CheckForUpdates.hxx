
#ifndef  INCLUDED_CHECK_FOR_UPDATES_HXX_INC
#define  INCLUDED_CHECK_FOR_UPDATES_HXX_INC

#include	"common.hxx"
#include	"Version.hxx"
#include	"Update.hxx"
#include	<vector>
#include	<sstream>
#include	<string>
#include	"CurlAdaptor.hxx"
#include	<stdexcept>
#include	<MALLOC.h>
#include	"PatchingAlgorithmsManager.hxx"
#include	<ctime>
#include	"UpdaterErrorCodes.hxx"

#include	<boost/preprocessor/stringize.hpp>

//@TODO: include less
#include	<boost/thread.hpp>

//@TODO: remove if it works without
extern "C"
{
#include	<getmodules.h>
#include	<loadversion.h>
#include	<getversion.h>
}

//! \warning You may not consider CheckForUpdates as a polymorphic type (i.e.
//representable by a CurlAdaptor<CheckForUpdates>&).
//@TODO: this will use shared memory for everything, including the singleton
//storage, so we can share the same object across two processes
class UPDATER_IMPL_SRC_EXPORT_IMPORT CheckForUpdates
    : public CurlAdaptor<CheckForUpdates>
{
public:
    static CheckForUpdates &getInstance()
    {
        static CheckForUpdates check_for_updates;
        return check_for_updates;
    }

    bool check ()
    {
        curlPerform(check_for_updates_url);
        last_successful_updates_check_ = std::time(NULL);
        return !updates_.empty();
    }

    UpdatesVector const &getAvailableUpdates() const
    {
        return updates_;
    }

    unsigned int secondsFromLastUpdateCheck() const
    {
        boost::lock_guard<boost::mutex> lock(mutex_);
        return std::time(NULL) - last_successful_updates_check_;
    }

    boost::mutex &getUpdatesMutex() { return mutex_; }
private:
    CheckForUpdates (CheckForUpdates const &);
    CheckForUpdates &operator=(CheckForUpdates const &);
    ~CheckForUpdates() { }

    CheckForUpdates () : updates_(), mutex_()
    {
        if (!PatchingAlgorithmsManager::getInstance().areAny())
        {
            throw UpdaterException(
                    UpdaterErrorCodes::NoPatchingAlgorithmsAvailable);
        }

        std::stringstream ss;


        //@TODO: update this list
        //1. get list of modules + version
        //2. get scilab version
        //3. get a list of toolboxes + version
        //4. get list of updatemodules + version (this can include stuff such
        //as locales) (i.e. fake modules that would get updated as if they were
        //real modules)
        //5. get a listing of available updaters

        //1)
        //@!!!TODO: getmodules() is not thread-safe
        struct MODULESLIST *modules = getmodules();

        //looks like: "module1:1|0|0|0|module2:1|2|3|0|:

        //@!!!TODO: getversionmodule() is not thread-safe.
        char crt_string_name[1024];
        for ( int i = 0; i < modules->numberofModules; ++i ) {
            Version version;
            if (!getversionmodule(modules->ModuleList[i],
                    (int*)&version.major_,
                    (int*)&version.minor_,
                    (int*)&version.maintenance_,
                    crt_string_name,
                    (int*)&version.revision_) )
                throw UpdaterException(
                        UpdaterErrorCodes::CouldNotGetVersionOfModule)
                        (modules->ModuleList[i]);
            version.string_.assign(crt_string_name);
            ss << modules->ModuleList[i] << ':';
            version.serializeInto(ss);
        }
        ss << ':';

        //2)
        //E.g.: 5|3|1|0|

        //boost::unique_ptr would be nicer here, with a custom deallocator
        int scilab_version_array_size, *version_array;
        version_array = getScilabVersion(&scilab_version_array_size);
        if ( !version_array )
            throw UpdaterException(
                    UpdaterErrorCodes::CouldNotGetVersionOfModule)("Scilab");
        
        Version version;
        version.major_ = version_array[0];
        version.minor_ = version_array[1];
        version.maintenance_ = version_array[2];
        version.revision_ = version_array[3];

        // array returned by getScilabVersion() is allocated with MALLOC
        FREE(version_array);

        char *scilab_version_string = getScilabVersionAsString();
        version.string_.assign(scilab_version_string);

        //string returned by getScilabVersionAsString() is allocated with
        //strdup()
        //@TODO: this is removed temporarily because it causes heap problems
        //(probably caused by a statically linked CRT somewhere). fix this and
        //uncomment.
        //free(scilab_version_string);

        version.serializeInto(ss);


        //1 is the version of the format of the serialized data
        setPostData( "v=1&p=" + CurlEasy::urlencode( ss.str() ) +
                "&pl=" + CurlEasy::urlencode( BOOST_PP_STRINGIZE(
                        UPDATER_PLATFORM_STRING)));
        setUserAgent( "Scilab-Updater/1.0" );
    }


    UpdatesVector updates_;

    mutable boost::mutex mutex_;

    std::time_t last_successful_updates_check_;


    static const std::string check_for_updates_url;

    //called automatically by CurlAdaptor when we've finished
    //retrieving results from /check_for_updates
    void onDownloadComplete (std::string const &data)
    {
        boost::lock_guard<boost::mutex> lock(mutex_);

        updates_.clear();
        //6. parse http output to get a list of available updates and return
        try
        {
            std::stringstream ss(data);
            Update update;
            //are there any bytes left to read from the stream?
            while ( Update::unserializeFrom(ss, update) )
            {
                updates_.push_back(update);
            }
        } catch (UpdaterException const &)
        {
            //unserialization failed, clear updates_ and rethrow
            updates_.clear();
            throw;
        }
    }

    friend class CurlAdaptor<CheckForUpdates>;
};

#endif   /* ----- #ifndef INCLUDED_CHECK_FOR_UPDATES_HXX_INC  ----- */
