#if defined(_WIN32) && !defined(INCLUDED_WINDOWS2K_PATCHAPI_UPDATER_HXX)
#define INCLUDED_WINDOWS2K_PATCHAPI_UPDATER_HXX

#include "updater.hxx"
#include "updater_manager.hxx"
#include <string>
//#include <Windows.h>

// http://msdn.microsoft.com/en-us/library/bb417345.aspx
//! An updater that uses the patchapi from the windows sdk.
//! \note Windows SDK is only required for creating patches, not for applying.
class windows2k_patchapi_updater : public updater
{
public:
	static unsigned int UPDATER_ID; // 1
	windows2k_patchapi_updater()
	{
		//mspatcha = LoadLibrary("mspatcha.dll");

	}

	bool apply_update (std::string const &filename,
		std::string const &patchfile);

	unsigned int get_updater_id() const { return UPDATER_ID; }

	std::string get_updater_name () const { return "Windows PatchAPI Updater"; }

	//~windows2k_patchapi_updater() { FreeLibrary(mspatcha); }
//private:
	//HMODULE mspatcha;

};

#endif
