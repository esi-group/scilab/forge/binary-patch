#if !defined(INCLUDED_XDELTA_UPDATER_HXX)
#define INCLUDED_XDELTA_UPDATER_HXX

#include "updater.hxx"
#include "updater_manager.hxx"
#include <string>

class xdelta_updater : public updater
{
public:
	static unsigned int UPDATER_ID;

	bool apply_update (std::string const &filename,
		std::string const &patchfile);

	unsigned int get_updater_id() const { return UPDATER_ID; }

	std::string get_updater_name () const { return "xdelta Updater"; }

};

#endif
