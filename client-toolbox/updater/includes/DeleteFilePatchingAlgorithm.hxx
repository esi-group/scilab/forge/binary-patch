
#ifndef  INCLUDED_UPDATER_DELETEFILEPATCHINGALGORITHM_HXX_INC
#define  INCLUDED_UPDATER_DELETEFILEPATCHINGALGORITHM_HXX_INC

#include	"PatchingAlgorithm.hxx"

class DeleteFilePatchingAlgorithm : public PatchingAlgorithm
{
public:
    static unsigned int ALGORITHM_ID;
	bool applyUpdate (std::string const &filename,
		std::string const &patchfile);
	std::string getPatchingAlgorithmName () const { return "File deleter"; }
	unsigned int getPatchingAlgorithmId() const { return ALGORITHM_ID; }

    virtual ~DeleteFilePatchingAlgorithm() { }

};

#endif
