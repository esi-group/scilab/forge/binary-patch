function installUpdater

    CWD = get_absolute_file_path("builder.sce")

    exec(CWD + '/config.sce')

    try
        v = getversion("scilab")
        // @TODO: s/3/5/
        if v(1) < 5 | v(2) < 3 then
            error;
        end
    catch
        error(gettext("Scilab 5.3 or more is required."));
    end

    clear v;

    if ~isdef('tbx_build_loader') then
        error(msprintf(gettext('%s module not installed.'), 'modules_manager'));
    end

    //@TODO: uncomment when we have macros
    //tbx_builder_macros(CWD);
    tbx_builder_src(CWD);
    tbx_builder_gateway(CWD);
    tbx_build_loader(TOOLBOX_NAME, CWD);
    tbx_build_cleaner(TOOLBOX_NAME, CWD);

endfunction

m=mode();
mode(-1);
installUpdater;
mode(m);
clear m installUpdater;
