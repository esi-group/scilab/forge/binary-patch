
#include	"sci_get_updates.hxx"

#include	<sciprint.h>
#include	<api_scilab.h>
#include	<stack-c.h>
#include	<Scierror.h>
#include	<localization.h>

#include	<stdexcept>

#include	"CheckForUpdates.hxx"
#include	"Update.hxx"
#include	"UpdaterThread.hxx"
#include	"UnappliedUpdatesStore.hxx"
#include	"UpdaterErrorCodes.hxx"

extern "C" int sci_get_updates (char *fname)
{
    CheckLhs(0, 1);
    CheckRhs(0, 0);

    try
    {
        //Easier to ask forgiveness than permission
        UpdaterThread::getInstance().setExpectingDownloadConfirmation(
                false);
        UpdaterThread::ErrorCode ec = 
            UpdaterThread::getInstance().runPeriodicalUpdatesChecker();
       
        //couldn't start thread
        if (ec != UpdaterThread::Ok && ec != UpdaterThread::ThreadIsRunning)
        {
            throw std::runtime_error(
                    UpdaterThread::getInstance().getMessageFromErrorCode(ec));
        }
       
        //if ec == UpdaterThread::Ok, it means we've just started the thread,
        //so it already does what it's supposed to, no need for other work.

        //thread was running already, ask it to check for updates
        if (ec == UpdaterThread::ThreadIsRunning)
        {
            ec = UpdaterThread::getInstance().forceUpdatesCheck();
            if (ec != UpdaterThread::Ok)
            {
                if (ec == UpdaterThread::ThreadBusy &&
                    UpdaterThread::getInstance().isWaitingDownloadConfirmation())
                {
                    UpdaterThread::getInstance().allowDownloadUpdates();
                }
                else
                {
                    throw std::runtime_error(
                        UpdaterThread::getInstance().getMessageFromErrorCode(ec));
                }
            }
        }
        sciprint("%s\n", _("Will do!"));
    }
    catch (std::runtime_error const &e)
    {
        Scierror(999, e.what());
    }

    return 0;
}
