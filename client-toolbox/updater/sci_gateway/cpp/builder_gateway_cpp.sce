function UpdaterBuildCppGateway
    
    CWD = get_absolute_file_path('builder_gateway_cpp.sce');
    exec(CWD + '/../../config.sce');

    UPDATER_CFLAGS = '-I' + ..
        get_absolute_file_path('builder_gateway_cpp.sce')+'../../includes' + ..
        ' -I' + BOOST_INC_DIR + ..
        ' -DUPDATER_PLATFORM_STRING=' + UPDATER_PLATFORM_STRING;

    SRC_LIB_PATH = CWD + '../../src/cpp/';
    LIB_PATH = CWD + '../../libs/';

    LIBS = [
        LIB_PATH + 'libcurl' ..
        SRC_LIB_PATH + 'libcommon/libcommon' ..
        SRC_LIB_PATH + ..
            'libpatching_algorithms_manager/libpatching_algorithms_manager' ..
        SRC_LIB_PATH + 'libupdater_impl/libupdater_impl' ..
        ];

    //names start_periodical_updates_checker and
    //stop_periodical_updates_checker were too long so they were shortened
    tbx_build_gateway('updater', ..
      [..
        'list_patching_algorithms', 'sci_list_patching_algorithms'; ..
        'check_for_updates', 'sci_check_for_updates'; ..
        'download_updates', 'sci_download_updates'; ..
        'apply_downloaded_updates', 'sci_apply_downloaded_updates'; ..
        'start_updates_checker', ..
            'sci_start_periodical_updates_checker'; ..
        'stop_updates_checker', .. 
            'sci_stop_periodical_updates_checker'; ..
        'get_updates', 'sci_get_updates'; ..
        ], ..
       ['sci_check_for_updates.cpp', ..
        'sci_download_updates.cpp', ..
        'sci_apply_downloaded_updates.cpp', ..
        'sci_list_patching_algorithms.cpp', ..
        'sci_start_periodical_updates_checker.cpp', ..
        'sci_stop_periodical_updates_checker.cpp', ..
        'sci_get_updates.cpp', ..
        'dllmain.cpp'], ..
        CWD, LIBS, UPDATER_BOOST_LDFLAGS, UPDATER_CFLAGS);
endfunction

UpdaterBuildCppGateway;
clear UpdaterBuildCppGateway;
