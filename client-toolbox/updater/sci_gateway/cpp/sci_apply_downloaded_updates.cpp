
#include	"sci_apply_downloaded_updates.hxx"
#include	"UnappliedUpdatesStore.hxx"
#include	"common.hxx"
#include	"UpdaterErrorCodes.hxx"

#include	<localization.h>
#include	<Scierror.h>
#include	<stack-c.h>
#include	<api_scilab.h>
#include	<sciprint.h>

extern "C" int sci_apply_downloaded_updates (char *fname)
{
    CheckRhs(0, 0);
    CheckLhs(0, 1);

    UnappliedUpdatesStore updates_store;
    if (!updates_store.open(true))
    {
        Scierror(999, cpp_printf(_("Couldn't open file: %s"),
                    updates_store.getPath()).c_str() );
    }
    else
    {
        try
        {
            updates_store.markUpdatesToBeApplied();
            sciprint("Updates will be applied when scilab is restarted.\n");
        } catch (UpdaterException const &e)
        {
            Scierror(999, message_from_exception(e).c_str());
        }
    }
    return 0;
}
