#include	"sci_check_for_updates.hxx"

#include	<sciprint.h>
#include	<api_scilab.h>
#include	<stack-c.h>
#include	<Scierror.h>
#include	<stdexcept>
#include	<localization.h>
#include	<vector>
#include	<memory>

#include	"CheckForUpdates.hxx"
#include	"Update.hxx"
#include	"UpdaterThread.hxx"
#include	"UnappliedUpdatesStore.hxx"
#include	"UpdaterErrorCodes.hxx"

// only for throw_if_downloaded_unapplied_updates()
#include	"DownloadUpdates.hxx"

//@TODO: turn this into sci_check_for_updates_pretty
//we want sci_check_for_updates to return a matrix of updates which can be
//passed to sci_download_updates
extern "C" int sci_check_for_updates (char *fname)
{
    CheckRhs(0, 0);
    CheckLhs(0, 1);


    try
    {
        //we have downloaded, unapplied updates
        throw_if_downloaded_unapplied_updates();

        bool work_on_this_thread = true;
        std::auto_ptr<UpdaterThread::ScopedResume> auto_resumer;
        if (UpdaterThread::getInstance().isRunning())
        {
            //after check returns, permission for updater thread to resume working
            //is granted only after this object runs out of scope
            auto_resumer = std::auto_ptr<UpdaterThread::ScopedResume>(
                    new UpdaterThread::ScopedResume);
            UpdaterThread::ErrorCode ec = 
                UpdaterThread::getInstance().forceUpdatesCheckAndWait(true);
            if (ec != UpdaterThread::Ok)
            {
                throw std::runtime_error(
                    UpdaterThread::getInstance().getMessageFromErrorCode(ec));
            }
            work_on_this_thread = false;
        }

#ifndef NDEBUG
        if (!work_on_this_thread)
            sciprint("%s\n", _("Checking for updates in updater thread..."));
        else
            sciprint("%s\n", _("Checking for updates..."));
#else
        sciprint("%s\n", _("Checking for updates..."));
#endif

        if (work_on_this_thread)
        {
            CheckForUpdates::getInstance().check();
        }

        UpdatesVector const &updates =
            CheckForUpdates::getInstance().getAvailableUpdates();

        //@TODO: return a matrix in the form [[name ver1 ver2..]; [name
        //ver1..]]
        if (!updates.empty())
            sciprint("=======================================================\n");
        for (UpdatesVector::const_iterator it = updates.begin();
                it != updates.end(); ++it)
        {
            sciprint(_("\tFound update for ``%s'' from %s to %s\n"),
                    it->module_name.c_str(),
                    std::string(it->old_version).c_str(),
                    std::string(it->new_version).c_str());
        }
        if (!updates.empty())
        {
            sciprint("=======================================================\n");
            sciprint(_("Please use download_updates();"
                    "apply_downloaded_updates(); "
                    "to get Scilab up-to-date.\n"
                    "To simplify this process, see ``help update''.\n"));
        }
        else
        {
            sciprint(_("No updates found.\n"));
        }

    }
    catch (CurlException const &e)
    {
        Scierror(999, _("Error checking for updates: %s"), e.what());
    }
    catch (UpdaterException const &e)
    {
        Scierror(999, message_from_exception(e).c_str());
    }
    catch (std::runtime_error const &e)
    {
        Scierror(999, e.what());
    }

    return 0;
}
