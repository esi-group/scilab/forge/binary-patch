
#include	"sci_stop_periodical_updates_checker.hxx"

#include	<sciprint.h>
#include	<api_scilab.h>
#include	<stack-c.h>
#include	<Scierror.h>
#include	<localization.h>

#include	"UpdaterThread.hxx"

extern "C" int sci_stop_periodical_updates_checker (char *fname)
{
    CheckLhs(0,1);
    CheckRhs(0,0);

    if ( !UpdaterThread::getInstance().stopPeriodicalUpdatesChecker() )
    {
        Scierror(999, _("Periodical updates checker was not running."));
    }
    else sciprint("Periodical updates checker stopped.\n");
    return 0;
}
