
#include	"TemporaryDirectory.hxx"
#include	"common.hxx"
#include	"UpdaterErrorCodes.hxx"
#include	<cstdlib>
#include	<stdexcept>

#if  defined(_WIN32)
#include	<Windows.h> // used for GetTempPath, DeleteFileA, FormatMessageA
#else
#include	<cstdio>
#include	<unistd.h>
#include	<sys/stat.h> // mkdir
#include	<sys/types.h> // mkdir
#endif
#include	<cstdlib> // std::rand

////////////////////////////////////////////////////////////
//Implementation of class TemporaryDirectory//
////////////////////////////////////////////////////////////


static std::string rand_alpha (unsigned int size)

{
    static const char charset[] = "abcdefghijklmnopqrstuvwxyz";
    std::string ret; ret.reserve(size);
    for (unsigned int i = 0; i < size; ++i)
    
    {
        ret += charset[std::rand()%(sizeof(charset)-1)];
    }
    return ret;
}

//@TODO: srand(time(NULL)) somewhere, otherwise it keeps generating the same
//folders
TemporaryDirectory::TemporaryDirectory() : path_(), delete_on_destruction_(true)
{

    try
    {
#if  defined(_WIN32)
       
        char tmp_dir_pref[1024];
        if (!GetTempPathA(sizeof(tmp_dir_pref), tmp_dir_pref))
            throw UpdaterException(UpdaterErrorCodes::CouldNotGetTempPath,
                    true);


        bool again = true;
        while (again)
        {
            again = false;

            //create a random subdirectory
            path_ = tmp_dir_pref+rand_alpha(10)+"\\";

            if (!CreateDirectoryA(path_.c_str(), NULL))
            {
                if (GetLastError() == ERROR_ALREADY_EXISTS)
                {
                    again = true;
                    continue;
                }
                throw UpdaterException(
                        UpdaterErrorCodes::CouldNotCreateDirectory, true)(
                            path_);
            }
        }
    
        //MessageBoxA(NULL, ( "Created "+path_ ).c_str(), "Note", MB_OK);

#else      /* -----  not DEFINED(_WIN32)  ----- */
       
        //@TODO: is there a mkdtemp() in OS X? mkdtemp() is not POSIX.
        static const char tmpdir_template[] = "scilab-XXXXXX";
        std::vector<char> p(tmpdir_template,
                tmpdir_template+sizeof(tmpdir_template));
        path_ = mkdtemp(&p[0]);

/*         char tmp_dir_pref[] = "/tmp";
 *         path_ = tmp_dir_pref+("/"+rand_alpha(10))+"/";
 *         bool again = true;
 *         while (again)
 *         {
 *             again = false;
 *             if (mkdir(path_.c_str(), 0755) == -1)
 *             {
 *                 if (errno == EEXIST)
 *                 {
 *                     again = true;
 *                     continue;
 *                 }
 *                 throw UpdaterException(
 *                         UpdaterErrorCodes::CouldNotCreateDirectory, true)(
 *                             path_);
 *             }
 *         }
 */
#endif     /* -----  not DEFINED(_WIN32)  ----- */

    }
    catch (...)
    {
        //creating temp dir didn't work, cleanup and rethrow the exception
        deleteDir();
        throw;
    }
}

void TemporaryDirectory::keepIt() { delete_on_destruction_ = false; }

void TemporaryDirectory::deleteIt()
{
    delete_on_destruction_ = false;
    deleteDir();
}

//@TODO: Make this work even if the directory is not empty (recursive delete)
void TemporaryDirectory::deleteDir()
{
#ifdef _WIN32
    //MessageBoxA(NULL, ( "Deleted "+path_ ).c_str(), "Note", MB_OK);
    DeleteFileA(path_.c_str());
#else
    unlink(path_.c_str());
#endif
}

TemporaryDirectory::~TemporaryDirectory()
{
    if (delete_on_destruction_ && !path_.empty())
        deleteDir();
}

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
