
#include	"Update.hxx"
#include	<sstream>
#include	<cassert>
#include	<istream>
#include	<ostream>
#include	"Version.hxx"
#include	"common.hxx"
#include	"UpdaterErrorCodes.hxx"

////////////////////////////////////////////////////////////
//Implementation of class Update//
////////////////////////////////////////////////////////////

void Update::serializeInto (std::ostream &ss) const
{
    //serialize module name
    ss << module_name << '|';
    //make sure module name doesn't contain |, otherwise we can't serialize
    assert(module_name.find('|') == std::string::npos);

    //serialize old version
    {
        std::stringstream version_string;
        old_version.serializeInto(version_string);
        ss << version_string.str() /* << '|' */;
    }

    //serialize new version
    {
        std::stringstream version_string;
        new_version.serializeInto(version_string);
        ss << version_string.str() /* << '|' */;
    }
}

bool Update::unserializeFrom (std::istream &ss, Update &update)
{
    if (!std::getline(ss, update.module_name, '|'))
    {
        if (ss.eof())
            return false;
        throw UpdaterException(UpdaterErrorCodes::UpdateDeserializationFailed);
    }
    update.old_version = Version::unserializeFrom(ss);
    update.new_version = Version::unserializeFrom(ss);
    return true;
}

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
