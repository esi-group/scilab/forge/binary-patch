#include	"bspatch.hxx"

#include	"BsdiffPatchingAlgorithm.hxx"
#include	"PatchingAlgorithmsManager.hxx"
#include	<cstdio>
#include	<cstdlib>
#include	<vector>

#ifdef _WIN32
#include	<Windows.h>
#else
#include	<unistd.h>
#include	<sys/types.h>
#endif

//@TODO: write right number
unsigned int BsdiffPatchingAlgorithm::ALGORITHM_ID = 4;

class Bspatch
{
public:
    Bspatch () : fh_(NULL), ofh_(NULL) { }

    bool apply (std::string const &filename,
            std::string const &patchfile)
    {
        MBSPatchHeader header;
        FILE *fh = std::fopen(patchfile.c_str(), "rb");
        if (!fh) return false;

        FILE *ofh = std::fopen(filename.c_str(), "rb");
        if (!ofh) return false;

        if (MBS_ReadHeader(fh, &header) != UPDATER_BSPATCH_OK) return false;

        //read the contentsw of the old file into old_file
        std::vector<unsigned char> old_file(
                header.slen < 4096 ? 4096 : header.slen);
        std::size_t current_offset = 0, bytes_read;
        while ( (bytes_read = 
                    fread(&old_file[current_offset], 1, 4096, ofh)) > 0)
        {
            current_offset += bytes_read;
            
            // source file bigger than indicated in the patch
            if (current_offset > header.slen) return false;
        }

        //a fread() call failed
        if (!feof(ofh)) return false;

        // size of source file differs from the one in bsdiff
        if (current_offset != header.slen) return false;

        std::fclose(ofh);

        //to be sure, check header.scrc32 crc32 against &old_file[0]
        if (crc32(&old_file[0], old_file.size()) != header.scrc32)
        {
            return false;
        }
        
        //truncate file to header.dlen
        //this could theoretically cause race conditions (if the file is
        //modified by someone else right after we truncate it, but that
        //scenario is highly unlikely)
#ifdef _WIN32
        HANDLE wfh = CreateFileA(filename.c_str(),
                GENERIC_WRITE,
                0,
                NULL,
                OPEN_EXISTING,
                FILE_ATTRIBUTE_NORMAL,
                NULL);
        if (wfh == INVALID_HANDLE_VALUE) return false;

        //try to seek to header.dlen
        if ( !( SetFilePointer(wfh, header.dlen, NULL, FILE_BEGIN) 
                    == INVALID_SET_FILE_POINTER && GetLastError() != 0) )
        {
            //SetFilePointer succeeded
            //Note: this could still fail when extending and there's not enough
            //space on hdd, not sure how to test for that in winapi
            SetEndOfFile(wfh);
        }
        CloseHandle(wfh);
#else
        if (truncate(filename.c_str(), header.dlen) == -1)
            return false;
#endif
        ofh = std::fopen(filename.c_str(), "wb");
        if (!ofh) return false;

        if (MBS_ApplyPatch(&header, fh, &old_file[0], ofh) 
                != UPDATER_BSPATCH_OK)
            return false;

        return true;
    }

    ~Bspatch () { if (fh_) fclose(fh_); if (ofh_) fclose(ofh_); }
private:
    FILE *fh_, *ofh_;
};
 
bool BsdiffPatchingAlgorithm::applyUpdate (std::string const &filename,
            std::string const &patchfile)
{
    Bspatch bspatch;
    return bspatch.apply(filename, patchfile);
}

UPDATER_PATCHING_ALGORITHMS_MANAGER_SRC_EXPORT_IMPORT
PatchingAlgorithm *create_bsdiff_palg() 
    { return new BsdiffPatchingAlgorithm; }

namespace
{
    static bool bsdiff_palg_dummy_val =
        PatchingAlgorithmsManager::getInstance().registerPatchingAlgorithm(
                BsdiffPatchingAlgorithm::ALGORITHM_ID,
                &create_bsdiff_palg);
}
