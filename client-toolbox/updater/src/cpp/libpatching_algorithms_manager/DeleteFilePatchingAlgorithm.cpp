#include	"DeleteFilePatchingAlgorithm.hxx"
#include	"PatchingAlgorithmsManager.hxx"

#ifdef _WIN32
#include	<Windows.h>
#else
#include	<unistd.h>
#endif

unsigned int DeleteFilePatchingAlgorithm::ALGORITHM_ID = 254;

bool DeleteFilePatchingAlgorithm::applyUpdate (std::string const &filename,
    std::string const &patchfile)
{
#ifdef _WIN32
    return DeleteFileA(filename.c_str()) != 0;
#else
    return unlink(filename.c_str()) == 0;
#endif
}

UPDATER_PATCHING_ALGORITHMS_MANAGER_SRC_EXPORT_IMPORT
PatchingAlgorithm *create_deletefile_palg () 
    { return new DeleteFilePatchingAlgorithm; }

namespace
{
    static bool deletefile_palg_dummy_val = 
        PatchingAlgorithmsManager::getInstance().registerPatchingAlgorithm(
                DeleteFilePatchingAlgorithm::ALGORITHM_ID,
                &create_deletefile_palg);
}
