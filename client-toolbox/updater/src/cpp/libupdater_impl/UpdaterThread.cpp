
#include	<localization.h>
#include	<boost/date_time/posix_time/posix_time_types.hpp>
#include	<map>

#include	"UpdaterThread.hxx"
#include	"CheckForUpdates.hxx"
#include	"TemporaryDirectory.hxx"
#include	"DownloadUpdates.hxx"
#include	"UnappliedUpdatesStore.hxx"
#include	"UpdaterErrorCodes.hxx"
#include	"common.hxx"


#define DEBUG_STEPS_OF_UPDATERTHREAD_TO_FILE \
    "/home/stefan/binary-patch/updaterthread_debug.txt"

#ifdef DEBUG_STEPS_OF_UPDATERTHREAD_TO_FILE
#include	<time.h>
#include	<sys/time.h>
#include	<fstream>

void debug_step_of_updaterthread (std::string const &m)
{
    static std::ofstream f(DEBUG_STEPS_OF_UPDATERTHREAD_TO_FILE, std::ios::app);
    static struct timeval start_time;
    static bool first_call = true;
    if (first_call)
    {
        first_call = false;
        if (gettimeofday(&start_time, NULL)) return;
        f << std::endl;
    }

/*     time_t t; time(&t);
 *     std::string crt_time(ctime(&t));
 *     std::string::size_type s = crt_time.find('\n');
 *     if (s == std::string::npos) s = crt_time.size();
 *     f << "[" << crt_time.substr(0, s) << "] " << m << std::endl;
 */
    struct timeval now_time;
    if (gettimeofday(&now_time, NULL)) return;

    unsigned int time_in_ms = (now_time.tv_sec - start_time.tv_sec)*1000;
    time_in_ms += (now_time.tv_usec - start_time.tv_usec)/1000;
    f << "[" << time_in_ms << "] " << m << std::endl;
}
#define STEP_OF_UPDATERTHREAD(msg) debug_step_of_updaterthread(msg)
#else
#define STEP_OF_UPDATERTHREAD(msg)
#endif

//notifies, using the conditional variable `cond' and mutex `mutex' that `var'
//has changed state
inline void notify (
        bool &var,
        boost::mutex &mutex,
        boost::condition_variable &cond)
{
    {
        boost::lock_guard<boost::mutex> lock(mutex);
        var = true;
    }
    cond.notify_one();
}

inline void wait(
        bool &var,
        boost::mutex &mutex,
        boost::condition_variable &cond)
{
    boost::unique_lock<boost::mutex> lock(mutex);
    while (!var)
    {
        cond.wait(lock);
    }
    var = false;
}

//Constructing an object of this type will set the given boolean variable
//to true in the ctor and to false in the dtor, guaranteeing thread-safety
//via a provided mutex.
struct ScopedBool
{
    ScopedBool (bool &b, boost::mutex &m)
        : b_(b), m_(m)
    {
        boost::lock_guard<boost::mutex> lock(m_);
        b_ = true;
    }
    ~ScopedBool()
    {
        boost::lock_guard<boost::mutex> lock(m_);
        b_ = false;
    }
private:
    bool &b_;
    boost::mutex &m_;
};

//Constructing an object of this type will set the given boolean variable to
//`newv' in the ctor and restore its to the old value in the dtor, guaranteeing
//thead-safety via a provided mutex.
struct ScopedBoolBackup
{
    ScopedBoolBackup (bool &b, boost::mutex &m, bool newv)
        : b_(b), m_(m), oldv(false)
    {
        boost::lock_guard<boost::mutex> lock(m_);
        oldv = b_;
        b_ = newv;
    }

    ~ScopedBoolBackup ()
    {
        boost::lock_guard<boost::mutex> lock(m_);
        b_ = oldv;
    }
private:
    bool &b_;
    boost::mutex &m_;
    bool oldv;
};

//verifies whether variable `var' is true or false, in a threadsafe way (using
//its mutex, `mutex')
inline bool verify(bool &var, boost::mutex &mutex)
{
    bool ret;
    {
        boost::lock_guard<boost::mutex> lock(mutex);
        ret = var;
    }
    return ret;
}

//same as verify(), but thread-safely resets the value to false afterward.
inline bool verify_reset (bool &var, boost::mutex &mutex)
{
    bool ret;
    {
        boost::lock_guard<boost::mutex> lock(mutex);
        ret = var;
        var = false;
    }
    return ret;
}

inline void set (bool &var, boost::mutex &mutex, bool value)
{
    boost::lock_guard<boost::mutex> lock(mutex);
    var = value;
}

//@TODO: right order in intialization here and everywhere else please. g++
//should warn with -Wall -Wextra
UpdaterThread::UpdaterThread()
    : last_updater_exception_(UpdaterErrorCodes::Ok),
    is_running_(false),
    expecting_download_confirmation_(false),
    expecting_updates_check_report_(false),
    expecting_download_complete_report_(false),
    is_currently_checking_for_updates_(false),
    download_complete_report_(false),
    done_checking_for_updates_(false),
    resume_checking_for_updates_(false),
    //allowing_forced_updates_check_(false),
    is_forcing_updates_check_(false),
    is_asking_for_download_confirmation_(false),
    //confirmation_allow_download_response_(false),
    confirmation_allow_download_(false)
{ }

UpdaterThread::ErrorCode UpdaterThread::runPeriodicalUpdatesChecker()
{
    //do we have downloaded, but unapplied updates?
    {
        UnappliedUpdatesStore unapplied_updates_store;
        if (!unapplied_updates_store.open())
        {
            last_updater_exception_ = UpdaterException(
                    UpdaterErrorCodes::CouldNotOpenFile)(
                        unapplied_updates_store.getPath());
            return SeeUpdaterException;
        }
        if (unapplied_updates_store.areUnappliedUpdates(false))
        {
            return HaveDownloadedUnappliedUpdates;
        }
    }

    //thread already started
    if (isRunning()) return ThreadIsRunning;

    //start the thread
    try
    {
        thread_ = boost::thread( boost::ref(*this) );
    } catch (boost::thread_resource_error const &)
    {
        return ThreadResourceError;
        //@TODO.
    }
    return Ok;
}

bool UpdaterThread::isRunning()
{
    if (verify(is_running_, is_running_mutex_)) return true;
    else
    {
        //remnants of a thread
        if (thread_.joinable()) thread_.join();
        return false;
    }
}

bool UpdaterThread::stopPeriodicalUpdatesChecker()
{
    if (!verify(is_running_, is_running_mutex_))
    {
        if (thread_.joinable())
        {
            thread_.join();
            return true;
        }
        return false;
    }
    else
    {
        thread_.interrupt();
        thread_.join();
        return true;
    }
}

//This function is called by our updater thread
void UpdaterThread::operator()()
{
    ScopedBool is_running_scope_guard_(is_running_, is_running_mutex_);
    UpdaterException tmp_updater_exception;

    STEP_OF_UPDATERTHREAD("1");
    try
    {
        //@TODO: check if we have already downloaded updates, which were not
        //applied.
        bool are_updates = false;
        do
        {
            STEP_OF_UPDATERTHREAD("setting is_currently_checking_for_updates_");
            set(is_currently_checking_for_updates_,
                is_currently_checking_for_updates_mutex_,
                true);
            STEP_OF_UPDATERTHREAD("checking for updates");
            tmp_updater_exception = UpdaterException(UpdaterErrorCodes::Ok);
            try
            {
                are_updates =
                    CheckForUpdates::getInstance().check();
                STEP_OF_UPDATERTHREAD("checked for updates successfully");
            }
            catch (UpdaterException const &e)
            {
                //@TODO: handle updater error, maybe sleep for a while or try
                //again? depends on the error.
                tmp_updater_exception = e;
                STEP_OF_UPDATERTHREAD("error checking for updates");
            } catch (CurlException const &e)
            {
                tmp_updater_exception = UpdaterException(
                        UpdaterErrorCodes::ForwardCurlException)(e.what());
                STEP_OF_UPDATERTHREAD("curl error checking for updates");
            }
            STEP_OF_UPDATERTHREAD("done checking for updates");
            set(is_currently_checking_for_updates_,
                    is_currently_checking_for_updates_mutex_,
                    false);
            STEP_OF_UPDATERTHREAD("unset is_currently_checking_for_updates_");

            if (verify_reset(expecting_updates_check_report_,
                        expecting_updates_check_report_mutex_))
            {
                //let the main thread know that we've just checked for updates
                STEP_OF_UPDATERTHREAD("expecting updates check report");
                {
                    boost::lock_guard<boost::mutex> lock(
                            done_checking_for_updates_mutex_);
                    done_checking_for_updates_ = true;
                    done_checking_for_updates_result_ = tmp_updater_exception;
                }
                done_checking_for_updates_cond_.notify_one();
                STEP_OF_UPDATERTHREAD("done sending report, waiting resume");

                //wait for the main thread to allow us to continue work
                wait(resume_checking_for_updates_,
                    resume_checking_for_updates_mutex_,
                    resume_checking_for_updates_cond_);
                STEP_OF_UPDATERTHREAD("main thread allowed resume");
            }

            //there are no updates, wait a while and retry (wait until a
            //duration passes or until a check for updates is forced, whichever
            //comes first)
            if (!are_updates)
            {
                STEP_OF_UPDATERTHREAD("no updates, sleeping for a while before "
                       "checking again");
                boost::unique_lock<boost::mutex> lock(
                        is_forcing_updates_check_mutex_);
                allowing_forced_updates_check_ = true;
                while (is_forcing_updates_check_cond_.timed_wait(lock,
                            boost::posix_time::hours(30)) &&
                        !is_forcing_updates_check_);
                if (is_forcing_updates_check_)
                    STEP_OF_UPDATERTHREAD("checking updates forced by "
                            "main thread");
                else
                    STEP_OF_UPDATERTHREAD("some time passed, trying again");
                allowing_forced_updates_check_ = false;
                is_forcing_updates_check_ = false;
            }
        }
        while (!are_updates);

        STEP_OF_UPDATERTHREAD("found updates");
        on_found_updates();
    } catch (...)
    {
        STEP_OF_UPDATERTHREAD("unexpected exception, exiting updater thread");
        // make sure all exceptions are caught, otherwise a thread escaping out
        // of the call stack of this thread will terminate() our app
    }
    STEP_OF_UPDATERTHREAD("updater thread finished");
}

void UpdaterThread::on_found_updates()
{
    //we have updates available, ask the main thread for confirmation
    //before downloading (if configured so)
    if (verify(expecting_download_confirmation_,
                expecting_download_confirmation_mutex_))
    {
        STEP_OF_UPDATERTHREAD("expecting download confirmation");
        //announce main thread that we're asking for download confirmation
        notify(is_asking_for_download_confirmation_,
                is_asking_for_download_confirmation_mutex_,
                is_asking_for_download_confirmation_cond_);

        boost::unique_lock<boost::mutex> lock(
                confirmation_allow_download_mutex_);
        while (!confirmation_allow_download_)
        {
            confirmation_allow_download_cond_.wait(lock);
        }
        confirmation_allow_download_ = false;
        if (!confirmation_allow_download_response_)
            STEP_OF_UPDATERTHREAD("got confirmation: download not allowed");
        else
            STEP_OF_UPDATERTHREAD("got confirmation: download allowed");
        
        //download not allowed
        if (!confirmation_allow_download_response_) return;
    }

    //download the available updates
        //this type automatically sets is_currently_downloading_updates_ to
        //true on construction and to false on destruction
        //@TODO: is this ever used?
/*         ScopedBool is_downloading_updates(
 *                 is_currently_downloading_updates_,
 *                 is_currently_downloading_updates_mutex_);
 */

    
    STEP_OF_UPDATERTHREAD("downloading updates");
    UpdaterException download_exception(UpdaterErrorCodes::Ok);
    try
    {
        download_updates(
                CheckForUpdates::getInstance().getAvailableUpdates());
        STEP_OF_UPDATERTHREAD("updates downloaded successfully");
    }
    catch (UpdaterException const &e)
    {
        //@TODO: some way to signal failure in downloading updates
        //maybe a thread-safe LastErrorCode variable?
        //currently, download_complete_report_ is set to true even if
        //downloading has failed
        STEP_OF_UPDATERTHREAD("error downloading updates");
        download_exception = e;
    }

    //if main thread wants us to, report that the download has completed
    if (verify_reset(expecting_download_complete_report_,
                expecting_download_complete_report_mutex_))
    {
        STEP_OF_UPDATERTHREAD("expecting download complete report");
        {
            boost::lock_guard<boost::mutex> lock(
                    download_complete_report_mutex_);
            download_complete_report_ = true;
            download_complete_report_result_ = download_exception;
        }
        download_complete_report_cond_.notify_one();
        STEP_OF_UPDATERTHREAD("download complete reported");
    }

    // thread succeeded in downloading an update, no point in
    // checking for other updates again, because we'll end up downloading
    // the same update twice.
}

UpdaterThread::ErrorCode UpdaterThread::forceUpdatesCheck()
{
    if (verify(is_currently_checking_for_updates_,
                is_currently_checking_for_updates_mutex_))
        return AlreadyCheckingForUpdates;

    {
        boost::lock_guard<boost::mutex> lock(is_forcing_updates_check_mutex_);
        if (!allowing_forced_updates_check_)
            return ThreadBusy;
        is_forcing_updates_check_ = true;
    }
    is_forcing_updates_check_cond_.notify_one();
    return Ok;
}

UpdaterThread::ErrorCode UpdaterThread::forceUpdatesCheckAndWait(
        bool giveOwnershipUntilResume)
{
    is_currently_checking_for_updates_mutex_.lock();
    if (is_currently_checking_for_updates_)
    {
        //no need to use the mutex in modifying these bools, updater thread is
        //stuck while we hold lock on is_currently_checking_for_updates_mutex_
        expecting_updates_check_report_ = true;
        resume_checking_for_updates_ = !giveOwnershipUntilResume;
        is_currently_checking_for_updates_mutex_.unlock();
    }
    else
    {
        is_currently_checking_for_updates_mutex_.unlock();

        {
            boost::unique_lock<boost::mutex> lock(
                    is_forcing_updates_check_mutex_);
           
            if (!allowing_forced_updates_check_) return ThreadBusy;
            
            //same as above, no mutexes needed, the updater thread is stuck
            //while we hold lock `lock'.
            is_forcing_updates_check_ = true;
            expecting_updates_check_report_ = true;
            resume_checking_for_updates_ = !giveOwnershipUntilResume;
        }
        is_forcing_updates_check_cond_.notify_one();

    }

    boost::unique_lock<boost::mutex> lock(done_checking_for_updates_mutex_);
    while (!done_checking_for_updates_)
    {
        done_checking_for_updates_cond_.wait(lock);
    }
    done_checking_for_updates_ = false;
    if (done_checking_for_updates_result_.getErrorCode()
            == UpdaterErrorCodes::Ok)
    {
        return Ok;
    }
    last_updater_exception_ = done_checking_for_updates_result_;
    return SeeUpdaterException;
}

void UpdaterThread::resume()
{
    //@TODO: is a waiting_resume necessary?
    notify(resume_checking_for_updates_,
            resume_checking_for_updates_mutex_,
            resume_checking_for_updates_cond_);
}

UpdaterThread::ErrorCode
UpdaterThread::refuse_or_allow_download_updates (bool allow)
{
    {
        boost::lock_guard<boost::mutex> lock(
                is_asking_for_download_confirmation_mutex_);
        if (!is_asking_for_download_confirmation_)
            return WasNotAskingForDownloadConfirmation;
        is_asking_for_download_confirmation_ = false;
    }

    {
        boost::unique_lock<boost::mutex> lock(
                confirmation_allow_download_mutex_);
        confirmation_allow_download_response_ = allow;
    }
    confirmation_allow_download_cond_.notify_one();
    return Ok;
}

UpdaterThread::ErrorCode UpdaterThread::refuseDownloadUpdates()
{
    return refuse_or_allow_download_updates(false);
}

UpdaterThread::ErrorCode UpdaterThread::allowDownloadUpdates()
{
    return refuse_or_allow_download_updates(true);
}

UpdaterThread::ErrorCode UpdaterThread::allowDownloadUpdatesAndWait()
{
    //make the updater thread report when downloading updates is finished (only
    //for the duration of this scope)
    ScopedBool download_complete_report(expecting_download_complete_report_,
            expecting_download_complete_report_mutex_);
    
    allowDownloadUpdates();
   
    //wait until downloading updates is complete
    boost::unique_lock<boost::mutex> lock(download_complete_report_mutex_);
    while (!download_complete_report_)
    {
        download_complete_report_cond_.wait(lock);
    }
    download_complete_report_ = false;
    if (download_complete_report_result_.getErrorCode() 
            == UpdaterErrorCodes::Ok)
    {
        return Ok;
    }
    last_updater_exception_ = download_complete_report_result_;
    return SeeUpdaterException;
}

bool UpdaterThread::isWaitingDownloadConfirmation()
{
    boost::lock_guard<boost::mutex> lock(
            is_asking_for_download_confirmation_mutex_);
    return is_asking_for_download_confirmation_;
}

UpdaterThread::ErrorCode UpdaterThread::forceUpdatesCheckAndDownloadAndWait()
{
    //ask the updater thread to check for updates and pause until we resume it
    ErrorCode ec;
    if ( (ec = forceUpdatesCheckAndWait(true)) != Ok)
    {
        resume();
        return ec;
    }
    
    //make expecting_download_confirmation_=false for the duration of this
    //scope
    ScopedBoolBackup expecting_download_confirmation_backup(
        expecting_download_confirmation_,
        expecting_download_confirmation_mutex_,
        false);
    
    //make expecting_download_complete_report_=true for the duration of this
    //scope
    ScopedBool expecting_download_complete_report(
        expecting_download_complete_report_,
        expecting_download_complete_report_mutex_);

    {
        ScopedResume resume_when_outofscope;
        if (CheckForUpdates::getInstance().getAvailableUpdates().empty())
        {
            //@TODO: how does that even work? can any dtor work really be done
            //after return, or is ScopedReturn::~ScopedReturn called before
            //return?
            return NoUpdatesAvailable;
        }
        //we have updates available
    }

    // wait for downloads complete report
    boost::unique_lock<boost::mutex> lock(download_complete_report_mutex_);
    while (!download_complete_report_)
    {
        download_complete_report_cond_.wait(lock);
    }
    download_complete_report_ = false;
    
    if (download_complete_report_result_.getErrorCode() == UpdaterErrorCodes::Ok)
    {
        return Ok;
    }

    last_updater_exception_ = download_complete_report_result_;
    return SeeUpdaterException;
}

namespace
{
    static std::map<UpdaterThread::ErrorCode, std::string>
    initialize_messages ()
    {
        std::map<UpdaterThread::ErrorCode, std::string> msgs;
        msgs[UpdaterThread::Ok] = _("No error");
        msgs[UpdaterThread::ThreadNotRunning] = 
            _("Updater thread is not running");
        msgs[UpdaterThread::ThreadIsRunning] = 
            _("Updater thread is already running");
        msgs[UpdaterThread::ThreadResourceError] = 
            _("Not enough resources to work with an updater thread");
        //@TODO: somehow use the same message from the other place
        msgs[UpdaterThread::HaveDownloadedUnappliedUpdates] = 
            message_from_exception(UpdaterException(
                        UpdaterErrorCodes::HaveDownloadedUnappliedUpdates));
        msgs[UpdaterThread::AlreadyCheckingForUpdates] = 
            _("Updater thread is already checking for updates");
        msgs[UpdaterThread::WasNotAskingForDownloadConfirmation] = 
            _("Sending confirmation "
                "of downloading updates when not asked for one");
        msgs[UpdaterThread::NoUpdatesAvailable] = 
            _("There are no new updates available");
        msgs[UpdaterThread::ThreadBusy] = 
            _("Updater thread is busy doing other work");
        return msgs;
    }
}

std::string UpdaterThread::getMessageFromErrorCode (
        UpdaterThread::ErrorCode ec)
{
    static std::map<ErrorCode, std::string> msgs = initialize_messages();
    if (ec == SeeUpdaterException)
    {
        return message_from_exception(last_updater_exception_);
    }
    return msgs[ec];
}

void UpdaterThread::setExpectingDownloadConfirmation(bool val)
{
    boost::lock_guard<boost::mutex> lock(expecting_download_confirmation_mutex_);
    expecting_download_confirmation_ = val;
}
