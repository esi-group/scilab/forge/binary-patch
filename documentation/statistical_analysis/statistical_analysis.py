import subprocess
import os
import argparse

#algos = ['xdelta', 'patchapi', 'msdelta', 'courgette', 'bsdiff']
algos = ['xdelta', 'bsdiff', 'patchapi']

# Configuration starts here
######################################################################
PYTHON_PATH = 'python.exe'
PATCH_CREATOR_SCRIPT_PATH = '../../server/patch_creator.py'
SRC_FOLDER  = 'C:/Program Files/scilab-5.2.2'
DEST_FOLDER = 'C:/Program Files/scilab-5.3.1'
TMP_PATH = os.path.abspath('tmp/')
treat_algos = []
######################################################################
# Configuration ends here

if __name__ == '__main__':

    parser=argparse.ArgumentParser(description='Compare different binary '
                                            'patchers')
    parser.add_argument('-s', '--src', dest='src', action='store', type=str,
            default=SRC_FOLDER, help='The older version of Scilab')
    parser.add_argument('-d', '--dest', dest='dest', action='store', type=str,
            default=DEST_FOLDER, help='The newer version of Scilab')
    parser.add_argument('-S', '--skip', dest='skip', action='store_true',
            default=False, help='Skip generating patches and assume they have '
            'already been generated (i.e. only generate the spreadsheet)')
    args = parser.parse_args()
    src = args.src
    dest = args.dest
    skip_generation = args.skip

    assert(os.path.isfile(PATCH_CREATOR_SCRIPT_PATH))
    assert(os.path.isdir(SRC_FOLDER))
    assert(os.path.isdir(DEST_FOLDER))


    if not os.path.isdir(TMP_PATH):
        os.mkdir(TMP_PATH)

    if not skip_generation:
        if algos == treat_algos:
            first = True
        else:
            first = False
        for algo in treat_algos:
            print '#'*70
            print 'Generating patches using ``%s\'\'' % algo
            print '#'*70
            ##Generate full patch using the first algorithm
            ##Only generate binary patches using the rest
            args = [PYTHON_PATH, PATCH_CREATOR_SCRIPT_PATH, '-s', src, '-d',
                    dest, '-o', os.path.join(TMP_PATH, algo), '-a', algo]
            if not first:
                args = args[0:2] + ['-b'] + args[2:]
            else:
                first = False
            p=subprocess.Popen(args)
            p.wait()

    print '#'*70
    print 'Generating CSV file'
    print '#'

    #generate a csv file
    f=open('results.csv', 'w+')
    f.write( ',Source size,Destination size,'+ ','.join(algos)+'\n')

    # { file : {xdelta : N, patchapi: N, ... } .. }
    sizes = {}
    added_sizes = 0
    text_sizes = 0

    for algo in algos:
        up=open(os.path.join(TMP_PATH, algo, 'updates.txt'), 'r')
        subpatches=map(lambda x: x.split(':'),map(str.strip,up.readlines()))
        for patchtype, filename, patchfile in subpatches:
            #deleted file
            if patchtype == 254:
                continue
            #added file
            if patchtype == 255:
                added_sizes += os.path.getsize(os.path.join(dest, filename))
                continue
            #text file
            if patchtype == 2:
                text_sizes += os.path.getsize(os.path.join(TMP_PATH, algo,
                    patchfile))
                continue
            #binary file
            if filename not in sizes:
                sizes[filename] = {'src' : os.path.getsize(os.path.join(
                    src, filename)), 'dest' : os.path.getsize(os.path.join(
                        dest,filename))}
            sizes[filename][algo] = os.path.getsize(
                    os.path.join(TMP_PATH, algo, patchfile) )

    lines = 0
    for fn, data in sizes.iteritems():
        row = [fn, data['src'], data['dest']]
        for algo in algos:
            row.append(data[algo])
        f.write( ','.join(map(str,row)) + '\n')
        lines += 1
        if lines % 100 == 0:
            print 'Inserted %d lines' % lines

    f.write(',\n,\nAdded files: %d\nText patched files: %d\n' % (added_sizes,
        text_sizes))
    f.close()

