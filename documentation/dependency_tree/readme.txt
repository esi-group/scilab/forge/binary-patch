generate_dependency_tree.py is a small python script I've created which generates a dependency tree
out of scilab's project by looking through the .sln and .vc(x)proj files.
It outputs in the format of DOT language, so the typical way to use the script is:
1) open generate_dependency_tree.py and configure scilab path appropriately
2) python generate_dependency_tree.py|dot -Tpng -o tree.png
use python generate_dependency_tree.py -h to get help on how to limit the tree to certain projects only:
-f P = shows only the projects that P directly depends on
-t P = shows only the projects that depend on P directly
-m P = show only the projects that depend on P or P depends on (directly)
If multiple such options are specified, they are OR-ed.

Examples:

python generate_dependency_tree.py -m core -f libscilab|dot -Tpng -o core_libscilab.png
python generate_dependency_tree.py -m core|dot -Tpng -o core.png
python generate_dependency_tree.py -m polynomials|dot -Tpng -o polynomials.png
python generate_dependency_tree.py -m fileio|dot -Tpng -o fileio.png
python generate_dependency_tree.py -m xcos|dot -Tpng -o xcos.png
python generate_dependency_tree.py -t MALLOC|dot -Tpng -o depend_on_malloc.png
python generate_dependency_tree.py -m api_scilab|dot -Tpng -o api_scilab.png
python generate_dependency_tree.py|dot -Tpng -o all.png
