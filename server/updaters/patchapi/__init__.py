import updaters
import os.path

id = 5
name = 'patchapi'

def create(patch_creator_obj, fn, patch_fn, callback):
    from_=os.path.join(patch_creator_obj.from_folder, fn)
    to=os.path.join(patch_creator_obj.to_folder, fn)
    out=os.path.join(patch_creator_obj.output_folder, patch_fn)
    component = patch_creator_obj.current_component
    from_v = patch_creator_obj.crt_from_v
    to_v = patch_creator_obj.crt_to_v

    patch_creator_obj.process_manager.spawn([updaters.PATCHAPI_APP, from_, to,
        out], callback=lambda:callback(id, fn, from_v, to_v, patch_fn, component))
