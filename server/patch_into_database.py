import argparse
import web.db
import os.path
from patch_creator_config import (serverconfig, COMPONENTS_WIN, 
        COMPONENTS_POSIX, ALGORITHMS)

def query(q):
    return db.query(q)

class memoize:
    def __init__(self, f):
        self.memoize = {}
        self.f = f
    def __call__(self, *kargs):
        if tuple(kargs) not in self.memoize:
            self.memoize[tuple(kargs)] = self.f(*kargs)
        return self.memoize[tuple(kargs)]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Patch an application')
    parser.add_argument('-p', '--patch', action='store', help= \
            'The output directory of patch_creator.py', required=True)
    args = parser.parse_args()
    patchdir = args.patch

    db = web.db.database(
            dbn=serverconfig.dbconfig.DB_TYPE, db=serverconfig.dbconfig.DB_NAME,
            user=serverconfig.dbconfig.DB_USER, pw=serverconfig.dbconfig.DB_PASS,
            host=serverconfig.dbconfig.DB_HOST)


    @memoize
    def get_platform_id(platform):
        q = query('''
            SELECT platform_id as pid
            FROM platforms
            WHERE
            platform_short_name = %s LIMIT 1
            ''' % web.db.sqlquote(platform))

        if q:
            return q[0].pid
        else:
            long_platform_name = raw_input('Please enter a long name for ' \
                'platform ``%s\'\': ' % platform)
            query(
                    '''INSERT INTO platforms VALUES (NULL, %s, %s)'''
                    % (web.db.sqlquote(platform),
                        web.db.sqlquote(long_platform_name))
                    )
            return query('SELECT LAST_INSERT_ID() as pid')[0].pid

    platform = ''
    @memoize
    def get_component_id(component):
        q = query(
            '''SELECT component_id as cid
               FROM components
               WHERE component_name = %s
            ''' % web.db.sqlquote(component))
        if q:
            return q[0].cid
        else:
            if component == 'scilab':
                component_paths = ''
            elif platform == 'w86' or platform == 'w64':
                component_paths = ','.join(COMPONENTS_WIN[component])
            else:
                component_paths = ','.join(COMPONENTS_POSIX[component])

            
            query(
                '''INSERT INTO components VALUES (NULL, %s, %s)
                ''' % (web.db.sqlquote(component),
                       web.db.sqlquote(component_paths))
                )
            return query('SELECT LAST_INSERT_ID() as pid')[0].pid

    @memoize
    def get_version_id(major, minor, maintenance, revision=0, string=''):
        query_data = {
            'major' : int(major),
            'minor' : int(minor),
            'maintenance' : int(maintenance),
            'revision' : int(revision),
            'string' : web.db.sqlquote(string)
        }

        ##!!!IMPORTANT!!! FIXME TODO
        ##WE ARE NOT RELYING ON THE STRING OF THE VERSION ANYMORE
        ##PUT THE COMMENTED CODE BACK WHENEVER VERSION STRINGS ARE MORE
        ##CONSISTENT (I REMOVED THIS BECAUSE THE STABLE RELEASES ACTUALLY COME
        ##WITH A unstable-git VERSION STRING, WHICH DOESN'T MAKE MUCH SENSE)
        #################
        q = query('''
        SELECT version_id FROM versions 
        WHERE major={major} AND minor={minor} AND
            maintenance={maintenance} AND
            revision={revision} /*AND
            string={string}*/
        LIMIT 1
        '''.format(**query_data)
        )
        if q:
            return q[0].version_id
        else:
            query('''
            INSERT INTO versions VALUES (NULL, {major}, {minor},
                {maintenance}, {revision}, {string})
            '''.format(**query_data))
            return query('SELECT LAST_INSERT_ID() as vid')[0].vid
        
    @memoize
    def assure_algorithm_in_database(algid):
        q=query('''
            INSERT INTO algorithms VALUES (%d, %s)
            ''' % (algid, web.db.sqlquote(ALGORITHMS[algid]))
        )


    with open(os.path.join(patchdir, 'track.txt'), 'r') as f:
        platform = f.readline().strip()
        track = map(lambda x:x.strip().split(':'), f.readlines())
        patches = {}

        pid = get_platform_id(platform)
        #parse track.txt file into patches dictionary
        #patches = {componentid : [oldversion, newversion,
        #       {fn : {algorithmid : patch_fn}}]}
        for component, oldv, newv, algorithmid, fn, patch_fn in track:
            cid = get_component_id(component)
           
            patches.setdefault(cid, [oldv, newv, {}])
            patches[cid][2].setdefault(fn, {})
            
            assert(algorithmid not in patches[cid][2][fn])
            patches[cid][2][fn][algorithmid] = patch_fn
            assert(oldv == patches[cid][0])
            assert(newv == patches[cid][1])
           
        del track

        #start updating the database
        transaction = db.transaction()
        for cid, component_info in patches.iteritems():
            oldv = component_info[0]
            newv = component_info[1]
            #@TODO: get_version_id
            oldv = get_version_id(*oldv.split('|'))
            newv = get_version_id(*newv.split('|'))

            #add entry in table `updates'
            query('''
            INSERT INTO updates
            VALUES(NULL, {component_id},
                {platform_id}, {old_version_id}, {new_version_id})
            '''.format(
                platform_id=int(pid),
                component_id=int(cid),
                old_version_id=int(oldv),
                new_version_id=int(newv))
            )
            query('SET @LAST_UPDATE_ID = LAST_INSERT_ID()')

            for fn, algorithms_iter in component_info[2].iteritems():
                #add entry in table `patches'
                query('''
                INSERT INTO patches
                VALUES (NULL, @LAST_UPDATE_ID, {file_path})
                '''.format(
                    file_path=web.db.sqlquote(os.path.normpath(fn)))
                )
                query('SET @LAST_PATCH_ID = LAST_INSERT_ID()')

                for algorithmid, patch_fn in algorithms_iter.iteritems():
                    if patch_fn:
                        full_patch_fn = os.path.normpath(
                                os.path.join(patchdir, patch_fn))
                        patch_size = os.path.getsize(full_patch_fn)
                    else:
                        full_patch_fn = ''
                        patch_size = 0
                    
                    #make sure table `algorithms' is up-to-date
                    algorithmid = int(algorithmid)
                    assure_algorithm_in_database(algorithmid)

                    #add entry in table `concrete_patches'
                    query('''
                    INSERT INTO concrete_patches
                    VALUES (NULL, @LAST_PATCH_ID, {algorithm_real_id},
                        {stored_patch_filename}, {stored_patch_size})
                    '''.format(
                        algorithm_real_id=int(algorithmid),
                        stored_patch_filename=web.db.sqlquote(full_patch_fn),
                        stored_patch_size=patch_size
                    ))

        transaction.commit()


