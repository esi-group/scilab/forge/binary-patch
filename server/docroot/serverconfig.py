class dbconfig:
    DB_TYPE = 'mysql'
    DB_NAME = 'scilab_updates'
    DB_USER = 'root'
    DB_PASS = ''
    DB_HOST = '127.0.0.1'

#@TODO: remove table `algorithms'?
VALID_ALGORITHMS = set([1,2,3,4,254,255])
#@TODO: fill this properly
# a set of minimal required algorithms for the set
#REQUIRED_ALGORITHMS = set([1,254,255])
REQUIRED_ALGORITHMS = set([])

VALID_PLATFORMS = set(['w86', 'w64', 'l86', 'l64', 'm'])
